let newUser= {};

function createNewUser(){
    let firstName = prompt('Enter first name: ');
    let lastName = prompt('Enter last name: ');
    let birthday = prompt('Enter birthday dd.mm.yyyy: ');

    newUser = {
        firstName,
        lastName,
        birthday,
        getAge: function () {
            let birthDate = birthday.slice(-4);
            let now = new Date();
            let age = now.getFullYear() - birthDate;
            return age;
            },
        getPassword: function () {
            return (`${newUser.firstName[0]}`).toUpperCase() + (`${newUser.lastName}`).toLowerCase() + birthday.slice(-4);
             },
    };
    return newUser;
}

console.log(createNewUser(), newUser.getAge(), newUser.getPassword());


